<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

// 用户模块
Route::resource('/web/user', 'Web\UserController');
// 角色模块
Route::resource('/web/role', 'Web\RoleController');
// 菜单模块
Route::resource('/web/menu', 'Web\MenuController');


Route::post('/web/user/auth/login', 'Web\UserController@login');
Route::get('/web/user/auth/menu', 'Web\UserController@menu');
Route::get('/web/user/auth/info', 'Web\UserController@info');
Route::get('/web/menu/tree/list', 'Web\MenuController@tree');
