<?php

namespace App\Traits;

/**
 * Trait JsonResponse   自定义json响应
 * @package App\Traits
 * @updateTime 2020/9/23
 * @author Ypings
 */
trait JsonResponse
{
    /**
     * $successCode 正确状态码
     * @var int
     */
    protected $successCode = 1;

    /**
     * $errorCode 错误状态码
     * @var int
     */
    protected $errorCode = 0;

    /**
     * error
     * @param string $message
     * @param array $data
     * @param int $status
     * @return \Illuminate\Http\JsonResponse
     */
    public function error(string $message, array $data = [], int $status = 200)
    {
        $response = [
            'message' => $message ?? '请求出错',
            'code' => $this->errorCode,
            'data' => $data,
            'status' => $status
        ];
        return response()->json($response, 200);
    }

    /**
     * success
     * @param string $message
     * @param array $data
     * @param int $status
     * @return \Illuminate\Http\JsonResponse
     */
    public function success(string $message, array $data = [], int $status = 200)
    {
        $response = [
            'message' => $message ?? '请求成功',
            'code' => $this->successCode,
            'data' => $data,
            'status' => $status
        ];
        return response()->json($response, 200);
    }

}
