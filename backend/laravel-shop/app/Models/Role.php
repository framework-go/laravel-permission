<?php

namespace App\Models;

/**
 * Class Role   角色模型类
 * @package App\Models
 */
class Role extends BaseModel
{
    /**
     * $tableName
     * @var string
     */
    protected $tableName = 'roles';

    protected $hidden=['pivot'];

    /**
     * $listFields 列表查询字段
     * @var string[]
     */
    public $listFields = [
        'roles.id',
        'roles.name',
        'roles.remark',
        'roles.status',
        'roles.created_at'
    ];

    /**
     * $listFields 详情查询字段
     * @var string[]
     */
    public $readFields = [
        'roles.id',
        'roles.name',
        'roles.remark',
        'roles.status',
        'roles.created_at'
    ];

    /**
     * users    角色-用户（多对多）
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany('App\Models\User');
    }

    /**
     * menus    角色-菜单（多对多）
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function menus()
    {
        return $this->belongsToMany('App\Models\Menu');
    }

    /**
     * permissions  角色-菜单（多对多）
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function permissions()
    {
        return $this->belongsToMany('App\Models\Permission');
    }
}
