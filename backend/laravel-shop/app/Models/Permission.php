<?php

namespace App\Models;

/**
 * Class Permission 节点模型
 * @package App\Models
 */
class Permission extends BaseModel
{
    /**
     * $tableName 指定数据表名
     * @var string
     */
    protected $tableName = 'permissions';

    protected $hidden=['pivot'];

    /**
     * $listFields 列表查询字段
     * @var string[]
     */
    public $listFields = [
        'permissions.id',
        'permissions.menu_id',
        'permissions.name',
        'permissions.title',
        'permissions.method',
        'permissions.remark',
        'permissions.status',
        'permissions.created_at'
    ];

    /**
     * $readFields 详情页字段
     * @var string[]
     */
    public $readFields = [
        'permissions.id',
        'permissions.menu_id',
        'permissions.name',
        'permissions.title',
        'permissions.method',
        'permissions.remark',
        'permissions.status',
        'permissions.created_at'
    ];

    /**
     * menu 反向一对一
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function menu()
    {
        return $this->belongsTo(Menu::class);
    }

}
