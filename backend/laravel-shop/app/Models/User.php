<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Schema;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * Class User   管理员业务类
 * @package App\Models
 */
class User extends Authenticatable implements JWTSubject
{
    use SoftDeletes;

    use Notifiable;

    /**
     * $guarded
     * @var array
     */
    public $guarded = [];

    /**
     * $fillable
     * @var array
     */
    public $fillable = [];

    /**
     * $tableName 数据表的完整名称
     * @var string
     */
    protected $tableName = 'users';

    /**
     * $listOrder
     * @var string
     */
    public $listOrder = 'created_at';

    /**
     * $listFields 列表显示字段
     * @var string[]
     */
    public $listFields = [
        'users.id',
        'users.username',
        'users.name',
        'users.gender',
        'users.mobile',
        'users.login_count',
        'users.last_login_time',
        'users.last_login_ip',
        'users.status',
        'users.created_at'
    ];

    /**
     * $readFields
     * @var string[]
     */
    public $readFields = [
        'users.id',
        'users.username',
        'users.name',
        'users.gender',
        'users.mobile',
        'users.remark',
        'users.login_count',
        'users.last_login_time',
        'users.last_login_ip',
        'users.status',
        'users.created_at'
    ];

    /**
     * getTableColumn 获取数据表的所有字段
     * @return array
     */
    public function getTableColumn(): array
    {
        if (empty($this->tableName)) {
            return [];
        }
        return Schema::getColumnListing($this->tableName);
    }

    /**
     * roles 用户-角色（多对多）
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany('App\Models\Role');
    }

    /**
     * getJWTIdentifier
     * @return mixed|void
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * getJWTCustomClaims
     * @return array|void
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
