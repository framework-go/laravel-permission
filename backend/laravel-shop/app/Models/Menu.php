<?php

namespace App\Models;

/**
 * Class Menu   菜单模型类
 * @package App\Models
 */
class Menu extends BaseModel
{
    /**
     * $tableName 指定数据表名
     * @var string
     */
    protected $tableName = 'menus';

    /**
     * $listFields 列表查询字段
     * @var array
     */
    public $listFields = [
        '*'
    ];

    /**
     * $readFields 详情显示字段
     * @var array
     */
    public $readFields = [
        '*'
    ];

    /**
     * boot
     */
    protected static function boot()
    {
        parent::boot();
        // 创建监听事件
        static::creating(function (Menu $menu) {
            if (is_null($menu->pid) || $menu->pid == 0) {
                $menu->pid = 0;
                $menu->level = 1;
                $menu->paths = '-';
            } else {
                $menu->level = $menu->parent->level + 1;
                $menu->paths = $menu->parent->paths . $menu->pid . '-';
                self::where('id', $menu->parent->id)->update(['is_directory' => 1]);
            }
        });
        // 更新监听事件
        static::updating(function (Menu $menu) {
            if (is_null($menu->pid) || $menu->pid == 0) {
                $menu->level = 1;
                $menu->paths = '-';
            } else {
                $menu->level = $menu->parent->level + 1;
                $menu->path = $menu->parent->paths . $menu->pid . '-';
                // 更新父类
                self::where('id', $menu->parent->id)->update(['is_directory' => 1]);
            }
        });
    }

    /**
     * parent 反向一对一
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo('App\Models\Menu', 'pid', 'id');
    }

    /**
     * children 一对多
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function children()
    {
        return $this->hasMany('App\Models\Menu', 'pid', 'id');
    }

    /**
     * 获取器
     * getPathIdsAttribute 获取所有祖先类目的 ID 值
     * @return false|string[]
     */
    public function getPathsIdsAttribute()
    {
        return array_filter(explode('-', trim($this->paths, '-')));
    }

    /**
     * 获取器
     * getAncestorsAttribute 获取所有祖先类目并按层级排序
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getAncestorsAttribute()
    {
        return Menu::query()
            // 使用上面的访问器获取所有祖先类目 ID
            ->whereIn('id', $this->paths_ids)
            // 按层级排序
            ->orderBy('level')
            ->get();
    }

    /**
     * 获取器
     * getFullNameAttribute 获取以 - 为分隔的所有祖先类目名称以及当前类目的名称
     * @return mixed
     */
    public function getFullNameAttribute()
    {
        return $this->ancestors  // 获取所有祖先类目
        ->pluck('name') // 取出所有祖先类目的 name 字段作为一个数组
        ->push($this->name) // 将当前类目的 name 字段值加到数组的末尾
        ->implode(' - '); // 用 - 符号将数组的值组装成一个字符串
    }

    // public function get

    /**
     * roles 菜单-角色（多对多）
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany('App\Model\Role');
    }

    /**
     * permissions 菜单-权限（一对多）
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function permissions()
    {
        return $this->hasMany('App\Models\Permission');
    }
}
