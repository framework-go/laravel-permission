<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Traits\JsonResponse;

/**
 * Class BaseController 后台控制器基类
 * @package App\Http\Controllers\Admin
 */
class BaseController extends Controller
{
    use JsonResponse;

    /**
     * $service
     * @var null
     */
    protected $service = null;

    /**
     * show 详情
     * @param int $id
     * @return object
     */
    public function show(int $id)
    {
        $info = $this->service->getDetailById($id);

        return $this->success('获取成功', $info);
    }

    /**
     * destroy 删除
     * @param int $id
     * @return object
     */
    public function destroy(int $id)
    {
        $result = $this->service->deleteData($id);
        if ($result) {
            return $this->success('删除成功');
        } else {
            return $this->error('删除失败');
        }
    }
}
