<?php

namespace App\Http\Controllers\Web;

use App\Http\Requests\UserRequest;
use App\Services\User;
use Illuminate\Http\Request;

class UserController extends BaseController
{
    /**
     * UserController constructor.
     */
    public function __construct()
    {
        $this->service = User::getInstance();
    }

    /**
     * index 用户列表
     * @param UserRequest $request
     * @return object
     * @throws \Exception
     */
    public function index(UserRequest $request)
    {
        // 关键词匹配
        if (!empty($request->get('keyword'))) {
            $condition[] = ['users.username', 'like', '%' . $request->get('keyword') . '%'];
        }
        // 状态值
        if (!empty($request->get('status'))) {
            $condition[] = ['users.status', '=', $request->get('status')];
        }
        $list = $this->service->getList($condition ?? []);

        return $this->success('查询成功', $list);
    }

    /**
     * store 新增用户
     * @param UserRequest $request
     * @return object
     * @throws \Exception
     */
    public function store(UserRequest $request)
    {
        $this->service->insertData($request->all());

        return $this->success('新增成功', []);
    }

    /**
     * update 更新用户
     * @param UserRequest $request
     * @param int $id
     * @return object
     * @throws \Exception
     */
    public function update(UserRequest $request, int $id)
    {
        $this->service->updateData($id, $request->all());

        return $this->success('更新成功', []);
    }

    /**
     * login 后台登录接口
     *
     * @param UserRequest $request
     * @return void
     */
    public function login(UserRequest $request)
    {
        $result = $this->service->login($request->all());
        if ($result) {
            $list['token'] = 'Bearer ' . $result;
            return $this->success('登录成功', $list);
        } else {
            return $this->error('登录失败');
        }
    }

    /**
     * logout 注销登录
     * @param Request $request
     * @return object
     * @throws \Exception
     */
    public function logout(Request $request)
    {
        $this->service->logout();

        return $this->success('注销成功');
    }

    /**
     * info
     *
     * @return void
     */
    public function info()
    {
        return $this->success('获取成功', [
            'roles' => 'seo',
            'name' => '小混混',
            'avatar' => 'http://www.baidu.com',
            'menu' => $this->service->getUserMenus()
        ]);
    }
}
