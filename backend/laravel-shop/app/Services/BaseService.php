<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;

/**
 * 基础业务类
 * Class BaseService
 * @package App\Service
 */
class BaseService implements BaseInterface
{
    /**
     * $imgDomain 图片地址
     * @var string
     */
    public $imgDomain = "http://127.0.0.1/storage/uploads/image/";

    /**
     * $model 业务类所属模型
     * @var null
     */
    protected $model = null;

    /**
     * $instance
     * @var null
     */
    protected static $instance = null;

    /**
     * @return BaseService|null
     */
    public static function getInstance()
    {
        if (!static::$instance instanceof static) {
            static::$instance = new static();
        }
        return static::$instance;
    }

    /**
     * BaseService constructor.
     */
    protected function __construct()
    {
    }

    /**
     * __clone
     */
    protected function __clone()
    {
        // TODO: Implement __clone() method.
    }


    /**
     * getList 列表查询
     * @param array $condition
     * @return array
     * @throws \Exception
     */
    public function getList(array $condition): array
    {
        try {
            if (request()->get('model') == 'all') {
                if ($condition) {
                    $list = $this->model->where($condition)->orderBy($this->model->listOrder, 'desc')->get($this->model->listFields);
                } else {
                    $list = $this->model->orderBy($this->model->listOrder, 'desc')->get($this->model->listFields);
                }
            } else {
                if ($condition) {
                    $list = $this->model->where($condition)->orderBy($this->model->listOrder,'desc')->paginate(request()->get('list_rows', 10), $this->model->listFields, 'page', request()->get('page', 1));
                } else {
                    $list = $this->model->orderBy($this->model->listOrder, 'desc')->paginate(request()->get('list_rows', 10), $this->model->listFields, 'page', request()->get('page', 1));
                }
            }
        } catch (\Exception $exception) {
            throw new \Exception($exception->getMessage());
        }

        return objectToArray($list);
    }

    /**
     * getDetailById 查询数据详情
     * @param int $id
     * @return array
     * @throws \Exception
     */
    public function getDetailById(int $id): array
    {
        try {
            $info = $this->model->where('id', $id)->first($this->model->readFields);
            if (empty($info)) {
                throw new \Exception('id为' . $id . '记录不存在');
            }
        }catch (\Exception $exception) {
            throw new \Exception($exception->getMessage());
        }

        return objectToArray($info);
    }

    /**
     * insertData 新增数据
     * @param array $data
     * @return bool
     * @throws \Exception
     */
    public function insertData(array $data): bool
    {
        try {
            // 过滤非数据表字段
            $data = call_user_func([$this,'filterFields'], $data);
            // 新增数据
            $this->model->fill($data)->save();
        } catch (\Exception $exception) {
            throw new \Exception($exception->getMessage());
        }

        return true;
    }

    /**
     * updateData 更新数据
     * @param int $id
     * @param array $data
     * @return bool
     * @throws \Exception
     */
    public function updateData(int $id, array $data): bool
    {
        try {
            $data = call_user_func([$this,'filterFields'], $data);
            // 更新数据
            $this->model->where('id', $id)->update($data);
        } catch (\Exception $exception) {
            throw new \Exception($exception->getMessage());
        }

        return true;
    }

    /**
     * deleteData 删除数据
     * @param int $id
     * @return bool
     * @throws \Exception
     */
    public function deleteData(int $id): bool
    {
        DB::beginTransaction();
        try {
            $item = $this->model->where('id', $id)->first();
            if (empty($item)) {
                DB::rollBack();
                throw new \Exception('id为'. $id . '值不存在');
            }
            $this->model->where('id',$id)->delete();
        } catch (\Exception $exception) {
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }
        DB::commit();

        return true;
    }

    /**
     * filterFields 过滤非数据表字段
     * @param array $data
     * @return array
     */
    public function filterFields(array $data): array
    {
        // 过滤非数据表字段
        $illegal_fields = array_flip(array_diff(array_keys($data),$this->model->getTableColumn()));
        if (!empty($illegal_fields)) {
            foreach ($illegal_fields as $key => $value) {
                unset($data[$key]);
            }
        }

        return $data;
    }
}
