<?php


namespace App\Services;

/**
 * Class Permission 节点业务类
 * @package App\Service
 */
class Permission extends BaseService
{
    /**
     * Permission constructor.
     */
    public function __construct()
    {
        $this->model = new \App\Models\Permission();
    }

}
