<?php

namespace App\Services;

/**
 * Interface BaseInterface
 * @package App\Service
 */
interface BaseInterface
{
    /**
     * getList 列表
     * @param array $condition
     * @return mixed
     */
    public function getList(array $condition): array;

    /**
     * getDetailById 根据id查询记录详情
     * @param int $id
     * @return mixed
     */
    public function getDetailById(int $id): array;

    /**
     * insertData 新增数据
     * @param array $data
     * @return mixed
     */
    public function insertData(array $data): bool;

    /**
     * updateData 更新数据
     * @param int $id
     * @param array $data
     * @return mixed
     */
    public function updateData(int $id, array $data): bool;

    /**
     * deleteData 删除数据
     * @param int $id
     * @return mixed
     */
    public function deleteData(int $id): bool;
}
