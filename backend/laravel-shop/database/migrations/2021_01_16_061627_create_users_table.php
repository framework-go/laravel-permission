<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // 后台用户数据表
        Schema::create('users', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integerIncrements('id');
            $table->string('username', 30)->unique()->comment('用户名/用户名');
            $table->string('password',255)->comment('密码');
            $table->string('name', 30)->unique()->comment('姓名');
            $table->tinyInteger('gender')->default(1)->comment('1男2女');
            $table->string('mobile',30)->default('')->nullable()->comment('联系电话');
            $table->string('remark')->default('')->nullable()->comment('备注');
            $table->bigInteger('login_count')->default(0)->comment('登录次数');
            $table->timestamp('last_login_time')->nullable()->comment('最后一次登录时间');
            $table->string('last_login_ip')->nullable()->comment('最后一次登录的ip地址');
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
