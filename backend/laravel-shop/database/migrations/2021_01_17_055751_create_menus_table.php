<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // 菜单表
        Schema::create('menus', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integerIncrements('id');
            $table->unsignedInteger('pid')->default(0)->comment('上级');
            $table->tinyInteger('level')->default(1)->comment('层级');
            $table->string('paths', 50)->default('-')->comment('关系路径');
            $table->tinyInteger('is_directory')->default(0)->comment('是否有子类，0为否、1为是');
            $table->tinyInteger('type')->default(1)->comment('1菜单、2按钮');
            $table->string('title', 50)->comment('中文名称');
            $table->string('name', 50)->comment('英文标识');
            $table->string('icon', 30)->default('')->comment('icon图标');
            $table->tinyInteger('breadcrumb')->default(1)->comment('是否在breadcrumb面包屑中显示：0否、1是');
            $table->tinyInteger('noCache')->default(1)->comment('是否被 <keep-alive> 缓存：0否、1是');
            $table->tinyInteger('affix')->default(0)->comment('是否固定在tags-view中：0否、1是');
            $table->tinyInteger('alwaysShow')->default(1)->comment('一直显示根路由：0否、1是');
            $table->tinyInteger('hidden')->default(0)->comment('是否隐藏：0否、1是');
            $table->tinyInteger('outer_link')->default(0)->comment('是否为外链形式打开：0否、1是');
            $table->string('activeMenu', 100)->default('')->comment('高亮相对应的侧边栏');
            $table->string('component', 100)->default('')->comment('组件');
            $table->string('path', 100)->default('')->comment('路由地址');
            $table->string('url', 100)->default('')->comment('外链地址');
            $table->string('redirect', 100)->default('')->comment('重定向地址');
            $table->integer('sort')->default(0)->comment('排序');
            $table->string('remark', 255)->default('')->nullable()->comment('备注');
            $table->tinyInteger('status')->default(1)->comment('状态值');
            $table->timestamps();
            $table->softDeletes();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menus');
    }
}
