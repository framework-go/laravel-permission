<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMenuRoleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // 菜单-角色表
        Schema::create('menu_role', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->unsignedInteger('menu_id')->comment('菜单id');
            $table->unsignedInteger('role_id')->comment('角色id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu_role');
    }
}
