import pagination from "@/components/Pagination";
import tableColumn from '@/components/Table/column';
// import { getUrl } from '@/utils/url';

export default {
    components: {
        pagination,
        tableColumn
    },
    filters: {
        statusFilter(status) {
            const statusMap = {
                0: '禁用',
                1: '正常'
            }
            return statusMap[status]
        },
        stringToArray(value) {
            let newValue = []
            newValue[0] = value
            return newValue;
        }
    },
    data() {
        return {
            total: 0,   // 数据总数
            listQuery: {},  // 查询条件
            listLoading: false,
            tableData: [], // 数据
            popover_column: false, //列表项的show
        }
    },
    computed: {
        filterTableColumn() {
            return this.tableColumn.filter(v => v.show)
        },
    },
    created() {
        this.parseQuery();
    },
    mounted() {
        this.fetchData();
    },
    methods: {
        // 获取get参数
        parseQuery() {
            const query = Object.assign({}, this.$route.query);
            let listQuery = {
                page: 1,
                list_rows: 10,
            };
            // 判断是否有get传参
            if (query) {
                query.page && (query.page = Number(query.page));
                query.list_rows && (query.list_rows = Number(query.list_rows));
                listQuery = {
                    ...listQuery,
                    ...query,
                };
            }
            this.listQuery = listQuery;
        },
        // 菜单权限校验
        // authMenu(path) {
        //     let urls = getUrl()
        //     urls.push('/401', '/404', '/login')
        //     if (!urls.includes(path)) {
        //         this.$router.push('/401')
        //     }
        // },
        // 搜索
        handleFilter() {
            this.listQuery.page = 1;
            this.fetchData();
        },
        // 分页
        refresh() {
            this.fetchData();
        },
        // 删除数据
        handleDelete(id) {
            this.$confirm("此操作将永久删除该记录, 是否继续?", "提示", {
                confirmButtonText: "确定",
                cancelButtonText: "取消",
                type: "warning",
            }).then(() => {
                this.deleteData(id)
            })
        }
    }
}