import request from '@/utils/request'

// 菜单列表
export function listMenu(params) {
    return request({
        url: '/web/menu',
        method: 'get',
        params
    })
}

// 菜单详情
export function detailMenu(id) {
    return request({
        url: `/web/menu/${id}`,
        method: 'get'
    })
}

// 菜单新增
export function addMenu(data) {
    return request({
        url: '/web/menu',
        method: 'post',
        data
    })
}

// 菜单编辑
export function editMenu(id, data) {
    return request({
        url: `/admin/web/${id}`,
        method: 'put',
        data
    })
}

// 菜单删除
export function deleteMenu(id) {
    return request({
        url: `/admin/web/${id}`,
        method: 'delete'
    })
}

// 菜单树
export function getMenuTree(params)
{
    return request({
        url: '/web/menu/tree/list',
        method: 'get',
        params
    })
}