import request from '@/utils/request'

// 移除图片
export function deleteImage(name) {
    return request({
        url: `/admin/image/remove/${name}`,
        method: 'delete'
    })
}