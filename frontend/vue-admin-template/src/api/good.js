import request from '@/utils/request'

// 商品列表
export function listGood(params) {
    return request({
        url: '/admin/good',
        method: 'get',
        params
    })
}

// 商品详情
export function detailGood(id) {
    return request({
        url: `/admin/good/${id}`,
        method: 'get',
    })
}

// 新增商品
export function addGood(data) {
    return request({
        url: '/admin/good',
        method: 'post',
        data
    })
}

// 编辑商品
export function editGood(id, data) {
    return request({
        url: `/admin/good/${id}`,
        method: 'put',
        data
    })
}

// 删除商品
export function deleteGood(id) {
    return request({
        url: `/admin/good/${id}`,
        method: 'delete',
    })
}

// 获取指定商品的所有规格
export function getGoodSkus(id) {
    return request({
        url: `/admin/good/${id}/skus`,
        method: 'get',
    })
}