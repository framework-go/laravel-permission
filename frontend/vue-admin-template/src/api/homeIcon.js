import request from '@/utils/request'

// 首页icon列表
export function listHomeIcon(params) {
    return request({
        url: '/admin/homeIcon',
        method: 'get',
        params
    })
}

// 首页icon详情
export function detailHomeIcon(id) {
    return request({
        url: `/admin/homeIcon/${id}`,
        method: 'get',
    })
}

// 新增首页icon
export function addHomeIcon(data) {
    return request({
        url: '/admin/homeIcon',
        method: 'post',
        data
    })
}

// 编辑首页icon
export function editHomeIcon(id, data) {
    return request({
        url: `/admin/homeIcon/${id}`,
        method: 'put',
        data
    })
}

// 删除首页icon
export function deleteHomeIcon(id) {
    return request({
        url: `/admin/homeIcon/${id}`,
        method: 'delete',
    })
}