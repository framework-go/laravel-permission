import request from '@/utils/request'

// 商品分类列表
export function listGoodCategory(params) {
    return request({
        url: '/admin/goodCategory',
        method: 'get',
        params
    })
}

// 商品分类详情
export function detailGoodCategory(id) {
    return request({
        url: `/admin/goodCategory/${id}`,
        method: 'get'
    })
}

// 商品分类新增
export function addGoodCategory(data) {
    return request({
        url: '/admin/goodCategory',
        method: 'post',
        data
    })
}

// 商品分类编辑
export function editGoodCategory(id, data) {
    return request({
        url: `/admin/goodCategory/${id}`,
        method: 'put',
        data
    })
}

// 商品分类删除
export function deleteGoodCategory(id) {
    return request({
        url: `/admin/goodCategory/${id}`,
        method: 'delete'
    })
}

// 商品分类树
export function getGoodCategoryTree(params) {
    return request({
        url: '/admin/goodCategory/tree/list',
        method: 'get',
        params
    })
}