import request from '@/utils/request'

export function login(data) {
  return request({
    url: '/web/user/auth/login',
    method: 'post',
    data
  })
}

export function getInfo() {
  return request({
    url: '/web/user/auth/info',
    method: 'get',
  })
}

// 注销
export function logout() {
  return request({
    url: '/web/user/auth/logout',
    method: 'post'
  })
}

// 用户列表
export function listUser(params) {
  return request({
    url: '/web/user',
    method: 'get',
    params
  })
}

// 用户详情
export function detailUser(id) {
  return request({
    url: `/web/user/${id}`,
    method: 'get'
  })
}

// 用户新增
export function addUser(data) {
  return request({
    url: '/web/user',
    method: 'post',
    data
  })
}

// 用户编辑
export function editUser(id, data) {
  return request({
    url: `/web/user/${id}`,
    method: 'put',
    data
  })
}

// 用户删除
export function deleteUser(id) {
  return request({
    url: `/web/user/${id}`,
    method: 'delete'
  })
}
