import request from '@/utils/request'

// 轮播图列表
export function listBanner(params) {
    return request({
        url: '/admin/banner',
        method: 'get',
        params
    })
}

// 轮播图详情
export function detailBanner(id) {
    return request({
        url: `/admin/banner/${id}`,
        method: 'get',
    })
}

// 新增轮播图
export function addBanner(data) {
    return request({
        url: '/admin/banner',
        method: 'post',
        data
    })
}

// 编辑轮播图
export function editBanner(id, data) {
    return request({
        url: `/admin/banner/${id}`,
        method: 'put',
        data
    })
}

// 删除轮播图
export function deleteBanner(id) {
    return request({
        url: `/admin/banner/${id}`,
        method: 'delete',
    })
}