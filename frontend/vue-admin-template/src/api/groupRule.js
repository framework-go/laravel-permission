import request from '@/utils/request'

// 团购规则列表
export function listGroupRule(params) {
    return request({
        url: '/admin/groupRule',
        method: 'get',
        params
    })
}

// 团购规则详情
export function detailGroupRule(id) {
    return request({
        url: `/admin/groupRule/${id}`,
        method: 'get',
    })
}

// 新增团购规则
export function addGroupRule(data) {
    return request({
        url: '/admin/groupRule',
        method: 'post',
        data
    })
}

// 更新团购规则
export function editGroupRule(id, data) {
    return request({
        url: `/admin/groupRule/${id}`,
        method: 'put',
        data
    })
}

// 删除团购规则
export function deleteGroupRule(id) {
    return request({
        url: `/admin/groupRule/${id}`,
        method: 'delete',
    })
}