import request from '@/utils/request'

// 节点列表
export function listPermission(params) {
    return request({
        url: '/admin/permission',
        method: 'get',
        params
    })
}

// 节点详情
export function detailPermission(id) {
    return request({
        url: `/admin/permission/${id}`,
        method: 'get'
    })
}

// 节点新增
export function addPermission(data) {
    return request({
        url: '/admin/permission',
        method: 'post',
        data
    })
}

// 节点编辑
export function editPermission(id, data) {
    return request({
        url: `/admin/permission/${id}`,
        method: 'put',
        data
    })
}

// 节点删除
export function deletePermission(id) {
    return request({
        url: `/admin/permission/${id}`,
        method: 'delete'
    })
}
