import request from '@/utils/request'

// 角色列表
export function listRole(params) {
    return request({
        url: '/web/role',
        method: 'get',
        params
    })
}

// 角色详情
export function detailRole(id) {
    return request({
        url: `/web/role/${id}`,
        method: 'get',
    })
}

// 角色新增
export function addRole(data) {
    return request({
        url: '/web/role',
        method: 'post',
        data
    })
}

// 角色更新
export function editRole(id, data) {
    return request({
        url: `/web/role/${id}`,
        method: 'put',
        data
    })
}

// 角色删除
export function deleteRole(id) {
    return request({
        url: `/web/role/${id}`,
        method: 'delete',
    })
}

// 角色拥有的菜单列表
export function getRoleMenus(id) {
    return request({
        url: `/web/role/${id}/menus`,
        method: 'get'
    })
}
// 角色菜单下节点情况
export function getRoleMenuPermissions(roleId, menuId) {
    return request({
        url: `/web/role/${roleId}/menu/${menuId}/permissions`,
        method: 'get'
    })
}

// 保存角色拥有的节点权限
export function saveRolePermissions(data) {
    return request({
        url: '/web/role/rolePermission',
        method: 'post',
        data
    })
}