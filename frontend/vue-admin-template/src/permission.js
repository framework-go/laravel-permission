import router from './router'
import store from './store'
import { Message } from 'element-ui'
import NProgress from 'nprogress' // progress bar
import 'nprogress/nprogress.css' // progress bar style
import { getToken } from '@/utils/auth' // get token from cookie
import getPageTitle from '@/utils/get-page-title'

NProgress.configure({ showSpinner: false }) // NProgress Configuration

// 设置白名单
const whiteList = ['/login']

router.beforeEach(async(to, from, next) => {
  NProgress.start()
  // 设置小标题
  document.title = getPageTitle(to.meta.title)
  const hasToken = getToken()
  // 判断是否有token
  if (hasToken) {
    if (to.path === '/login') {
      next({ path: '/' })
      NProgress.done()
    } else {
      // 判断vuex中是否有对应的用户数据，如果没有则重新获取用户详情接口
      if (store.getters.name) {
        next()
      } else {
        try {
          // 获取用户详情接口，并返回该用户拥有的所有动态路由
          const { menu } = await store.dispatch('user/getInfo')
          // 将获取到的动态路径进行处理并存储到vuex中
          const accessRoutes = await store.dispatch('permission/generateRoutes', {menu})
          // 动态添加可访问路由
          router.addRoutes(accessRoutes)
          next({ ...to, replace: true })
        } catch (error) {
          await store.dispatch('user/resetToken')
          Message.error(error || 'Has Error')
          next(`/login?redirect=${to.path}`)
          NProgress.done()
        }
      }
    }
  } else {
    if (whiteList.indexOf(to.path) !== -1) {
      next()
    } else {
      next(`/login?redirect=${to.path}`)
      NProgress.done()
    }
  }
})

router.afterEach(() => {
  NProgress.done()
})
