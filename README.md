# laravel+vue权限管理系统

#### 介绍
基于laravel+vue实现前后端动态菜单权限管理系统



#### 开发环境

php7.4 + Mysql5.7 + Ngnix + redis



#### 数据表说明

##### 管理员表

users

| 字段            | 说明                 |
| --------------- | -------------------- |
| id              | 主键id               |
| username        | 用户名               |
| password        | 密码                 |
| name            | 姓名                 |
| gender          | 性别（1男、2女）     |
| mobile          | 手机号码             |
| remark          | 备注                 |
| login_count     | 登录次数             |
| last_login_time | 最后一次登录时间     |
| last_login_ip   | 最后一次登录的ip     |
| status          | 状态（1正常、0禁用） |
| created_at      | 创建时间             |
| updated_at      | 更新时间             |
| deleted_at      | 删除时间             |



##### 角色表

roles

| 字段       | 说明                 |
| ---------- | -------------------- |
| id         | 主键id               |
| name       | 角色名称             |
| remark     | 备注                 |
| status     | 状态（1正常、0禁用） |
| created_at | 创建时间             |
| updated_at | 更新时间             |
| deleted_at | 删除时间             |



##### 菜单表

menus

| 字段         | 说明         |
| ------------ | ------------ |
| id           | 主键id       |
| pid          | 上级id       |
| level        | 层级         |
| paths        | 路径id       |
| is_directory | 是否有子目录 |
| type         | 1菜单、2按钮 |
| title        | 菜单中文名称 |
| name         | 菜单英文名称 |
| icon         | 图标         |
| breadcrumb   | 面包屑导航   |
| noCache      | 是否缓存     |
| affix        |              |
| alwaysShow   |              |
| hidden       | 是否隐藏     |
| outer_link   | 外链地址     |
| activeMenu   | 高亮菜单地址 |
| component    | 组件         |
| path         | 路径         |
| url          |              |
| redirect     | 重定向地址   |
| sort         | 排序         |
| remark       | 备注         |
| status       | 1正常、0禁用 |
| created_at   | 创建时间     |
| updated_at   | 更新时间     |
| deleted_at   | 删除时间     |



##### 角色-用户关系表

role-user

| 字段    | 说明   |
| ------- | ------ |
| role_id | 角色id |
| user_id | 用户id |



##### 菜单-角色关系表

menu-role

| 字段    | 说明   |
| ------- | ------ |
| menu_id | 菜单id |
| role_id | 角色id |



#### 目录说明

后端项目路径：backend/laravel-shop

前端项目路径：frontend/vue-admin-template

sql文件路径：sql/laravel-shop.sql



#### 运行说明

前端：

1、切换到vue-element-tempalte目录下

2、执行npm install安装依赖

3、运行npm run dev



#### 配置文件修改说明







