/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : localhost:3306
 Source Schema         : laravel-shop

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 19/03/2021 15:06:12
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for failed_jobs
-- ----------------------------
DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for menu_role
-- ----------------------------
DROP TABLE IF EXISTS `menu_role`;
CREATE TABLE `menu_role`  (
  `menu_id` int(10) UNSIGNED NOT NULL COMMENT '菜单id',
  `role_id` int(10) UNSIGNED NOT NULL COMMENT '角色id'
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of menu_role
-- ----------------------------
INSERT INTO `menu_role` VALUES (13, 1);
INSERT INTO `menu_role` VALUES (14, 1);
INSERT INTO `menu_role` VALUES (15, 1);
INSERT INTO `menu_role` VALUES (17, 1);
INSERT INTO `menu_role` VALUES (19, 1);
INSERT INTO `menu_role` VALUES (22, 2);
INSERT INTO `menu_role` VALUES (23, 2);
INSERT INTO `menu_role` VALUES (24, 2);
INSERT INTO `menu_role` VALUES (26, 2);
INSERT INTO `menu_role` VALUES (28, 2);
INSERT INTO `menu_role` VALUES (22, 1);
INSERT INTO `menu_role` VALUES (23, 1);
INSERT INTO `menu_role` VALUES (24, 1);
INSERT INTO `menu_role` VALUES (26, 1);
INSERT INTO `menu_role` VALUES (28, 1);
INSERT INTO `menu_role` VALUES (4, 1);
INSERT INTO `menu_role` VALUES (5, 1);
INSERT INTO `menu_role` VALUES (6, 1);
INSERT INTO `menu_role` VALUES (8, 1);
INSERT INTO `menu_role` VALUES (10, 1);

-- ----------------------------
-- Table structure for menus
-- ----------------------------
DROP TABLE IF EXISTS `menus`;
CREATE TABLE `menus`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `pid` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '上级',
  `level` tinyint(4) NOT NULL DEFAULT 1 COMMENT '层级',
  `paths` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '-' COMMENT '关系路径',
  `is_directory` tinyint(4) NOT NULL DEFAULT 0 COMMENT '是否有子类，0为否、1为是',
  `type` tinyint(4) NOT NULL DEFAULT 1 COMMENT '1菜单、2按钮',
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '中文名称',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '英文标识',
  `icon` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT 'icon图标',
  `breadcrumb` tinyint(4) NOT NULL DEFAULT 1 COMMENT '是否在breadcrumb面包屑中显示：0否、1是',
  `noCache` tinyint(4) NOT NULL DEFAULT 1 COMMENT '是否被 <keep-alive> 缓存：0否、1是',
  `affix` tinyint(4) NOT NULL DEFAULT 0 COMMENT '是否固定在tags-view中：0否、1是',
  `alwaysShow` tinyint(4) NOT NULL DEFAULT 1 COMMENT '一直显示根路由：0否、1是',
  `hidden` tinyint(4) NOT NULL DEFAULT 0 COMMENT '是否隐藏：0否、1是',
  `outer_link` tinyint(4) NOT NULL DEFAULT 0 COMMENT '是否为外链形式打开：0否、1是',
  `activeMenu` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT '高亮相对应的侧边栏',
  `component` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT '组件',
  `path` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT '路由地址',
  `url` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT '外链地址',
  `redirect` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT '重定向地址',
  `sort` int(11) NOT NULL DEFAULT 0 COMMENT '排序',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT '备注',
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '状态值',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 34 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of menus
-- ----------------------------
INSERT INTO `menus` VALUES (1, 0, 1, '-', 1, 1, '权限管理', 'Auth', 'el-icon-s-help', 1, 1, 0, 1, 0, 0, NULL, '#', '#', NULL, NULL, 0, '权限管理模块', 1, '2021-01-17 09:56:30', '2021-01-17 14:01:26', NULL);
INSERT INTO `menus` VALUES (2, 1, 2, '-1-', 1, 1, '用户管理', 'User', NULL, 1, 1, 0, 1, 0, 0, '', '/auth/index', '/auth/user', NULL, '/auth/user/list', 0, NULL, 1, '2021-01-17 09:59:33', '2021-01-17 13:01:20', NULL);
INSERT INTO `menus` VALUES (3, 2, 3, '-1-2-', 1, 1, '用户列表', 'UserList', NULL, 1, 1, 0, 1, 1, 0, '/auth/user', '/auth/user/list', '/auth/user/list', NULL, NULL, 0, NULL, 1, '2021-01-17 10:00:54', '2021-01-17 10:49:49', NULL);
INSERT INTO `menus` VALUES (4, 3, 4, '-1-2-3-', 0, 2, '新增', 'UserList::add', NULL, 1, 1, 0, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, '2021-01-17 10:02:22', '2021-01-17 10:02:22', NULL);
INSERT INTO `menus` VALUES (5, 3, 4, '-1-2-3-', 0, 2, '编辑', 'UserList::edit', NULL, 1, 1, 0, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, '2021-01-17 10:49:25', '2021-01-17 10:49:25', NULL);
INSERT INTO `menus` VALUES (6, 3, 4, '-1-2-3-', 0, 2, '删除', 'UserList::delete', NULL, 1, 1, 0, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, '2021-01-17 10:49:49', '2021-01-17 10:49:49', NULL);
INSERT INTO `menus` VALUES (7, 2, 3, '-1-2-', 1, 1, '新增用户', 'UserAdd', NULL, 1, 1, 0, 1, 1, 0, '/auth/user', '/auth/user/add', '/auth/user/add', NULL, NULL, 0, NULL, 1, '2021-01-17 12:56:24', '2021-01-17 12:59:19', NULL);
INSERT INTO `menus` VALUES (8, 7, 4, '-1-2-7-', 0, 2, '提交', 'UserAdd::submit', NULL, 1, 1, 0, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, '2021-01-17 12:59:20', '2021-01-17 12:59:20', NULL);
INSERT INTO `menus` VALUES (9, 2, 3, '-1-2-', 1, 1, '编辑用户', 'UserEdit', NULL, 1, 1, 0, 1, 1, 0, '/auth/user', '/auth/user/edit', '/auth/user/edit/:id(\\d+)', NULL, NULL, 0, NULL, 1, '2021-01-17 13:01:21', '2021-01-17 13:16:14', NULL);
INSERT INTO `menus` VALUES (10, 9, 4, '-1-2-9-', 0, 2, '提交', 'UserEdit::submit', NULL, 1, 1, 0, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, '2021-01-17 13:16:14', '2021-01-17 13:16:14', NULL);
INSERT INTO `menus` VALUES (11, 1, 2, '-1-', 1, 1, '角色管理', 'Role', NULL, 1, 1, 0, 1, 0, 0, NULL, '/auth/index', '/auth/role', NULL, '/auth/role/list', 0, NULL, 1, '2021-01-17 13:20:30', '2021-01-17 13:27:01', NULL);
INSERT INTO `menus` VALUES (12, 11, 3, '-1-11-', 1, 1, '角色列表', 'RoleList', NULL, 1, 1, 0, 1, 1, 0, '/auth/role', '/auth/role/list', '/auth/role/list', NULL, NULL, 0, NULL, 1, '2021-01-17 13:21:22', '2021-01-17 13:24:26', NULL);
INSERT INTO `menus` VALUES (13, 12, 4, '-1-11-12-', 0, 2, '新增', 'RoleList::add', NULL, 1, 1, 0, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, '2021-01-17 13:23:41', '2021-01-17 13:23:41', NULL);
INSERT INTO `menus` VALUES (14, 12, 4, '-1-11-12-', 0, 2, '编辑', 'RoleList::edit', NULL, 1, 1, 0, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, '2021-01-17 13:24:05', '2021-01-17 13:24:05', NULL);
INSERT INTO `menus` VALUES (15, 12, 4, '-1-11-12-', 0, 2, '删除', 'RoleList::delete', NULL, 1, 1, 0, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, '2021-01-17 13:24:26', '2021-01-17 13:24:26', NULL);
INSERT INTO `menus` VALUES (16, 11, 3, '-1-11-', 1, 1, '新增角色', 'RoleAdd', NULL, 1, 1, 0, 1, 1, 0, '/auth/role', '/auth/role/add', '/auth/role/add', NULL, NULL, 0, NULL, 1, '2021-01-17 13:26:01', '2021-01-17 13:26:21', NULL);
INSERT INTO `menus` VALUES (17, 16, 4, '-1-11-16-', 0, 2, '提交', 'RoleAdd::submit', NULL, 1, 1, 0, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, '2021-01-17 13:26:21', '2021-01-17 13:26:21', NULL);
INSERT INTO `menus` VALUES (18, 11, 3, '-1-11-', 1, 1, '编辑角色', 'RoleEdit', NULL, 1, 1, 0, 1, 1, 0, '/auth/role', '/auth/role/edit', '/auth/role/edit/:id(\\d+)', NULL, NULL, 0, NULL, 1, '2021-01-17 13:27:02', '2021-01-17 13:27:42', NULL);
INSERT INTO `menus` VALUES (19, 18, 4, '-1-11-18-', 0, 2, '提交', 'RoleEdit::submit', NULL, 1, 1, 0, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, '2021-01-17 13:27:42', '2021-01-17 13:27:42', NULL);
INSERT INTO `menus` VALUES (20, 1, 2, '-1-', 1, 1, '菜单管理', 'Menu', NULL, 1, 1, 0, 1, 0, 0, NULL, '/auth/index', '/auth/menu', NULL, '/auth/menu/list', 0, NULL, 1, '2021-01-17 13:28:55', '2021-01-17 13:32:56', NULL);
INSERT INTO `menus` VALUES (21, 20, 3, '-1-20-', 1, 1, '菜单列表', 'MenuList', NULL, 1, 1, 0, 1, 1, 0, '/auth/menu', '/auth/menu/list', '/auth/menu/list', NULL, NULL, 0, NULL, 1, '2021-01-17 13:29:59', '2021-01-17 13:30:58', NULL);
INSERT INTO `menus` VALUES (22, 21, 4, '-1-20-21-', 0, 2, '新增', 'MenuList::add', NULL, 1, 1, 0, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, '2021-01-17 13:30:17', '2021-01-17 13:30:17', NULL);
INSERT INTO `menus` VALUES (23, 21, 4, '-1-20-21-', 0, 2, '编辑', 'MenuList::edit', NULL, 1, 1, 0, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, '2021-01-17 13:30:37', '2021-01-17 13:30:37', NULL);
INSERT INTO `menus` VALUES (24, 21, 4, '-1-20-21-', 0, 2, '删除', 'MenuList::delete', NULL, 1, 1, 0, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, '2021-01-17 13:30:58', '2021-01-17 13:30:58', NULL);
INSERT INTO `menus` VALUES (25, 20, 3, '-1-20-', 1, 1, '新增菜单', 'MenuAdd', NULL, 1, 1, 0, 1, 1, 0, '/auth/menu', '/auth/menu/add', '/auth/menu/add', NULL, NULL, 0, NULL, 1, '2021-01-17 13:31:44', '2021-01-17 13:32:17', NULL);
INSERT INTO `menus` VALUES (26, 25, 4, '-1-20-25-', 0, 2, '提交', 'MenuAdd::submit', NULL, 1, 1, 0, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, '2021-01-17 13:32:17', '2021-01-17 13:32:17', NULL);
INSERT INTO `menus` VALUES (27, 20, 3, '-1-20-', 1, 1, '编辑菜单', 'MenuEdit', NULL, 1, 1, 0, 1, 1, 0, '/auth/menu', '/auth/menu/edit', '/auth/menu/edit/:id(\\d+)', NULL, NULL, 0, NULL, 1, '2021-01-17 13:32:56', '2021-01-17 13:33:20', NULL);
INSERT INTO `menus` VALUES (28, 27, 4, '-1-20-27-', 0, 2, '提交', 'MenuEdit::submit', NULL, 1, 1, 0, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, '2021-01-17 13:33:20', '2021-01-17 13:33:20', NULL);
INSERT INTO `menus` VALUES (29, 1, 2, '-1-', 1, 1, '节点管理', 'Permission', NULL, 1, 1, 0, 1, 0, 0, NULL, '/auth/index', '/auth/permission', NULL, '/auth/permission/list', 0, NULL, 1, '2021-01-17 14:01:28', '2021-01-17 14:02:23', NULL);
INSERT INTO `menus` VALUES (30, 29, 3, '-1-29-', 1, 1, '节点列表', 'Permission', NULL, 1, 1, 0, 1, 1, 0, '/auth/permission', '/auth/permission/list', '/auth/permission/list', NULL, NULL, 0, NULL, 1, '2021-01-17 14:02:23', '2021-01-17 14:03:34', NULL);
INSERT INTO `menus` VALUES (31, 30, 4, '-1-29-30-', 0, 2, '新增', 'PermissionList::add', NULL, 1, 1, 0, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, '2021-01-17 14:03:03', '2021-01-17 14:03:03', NULL);
INSERT INTO `menus` VALUES (32, 30, 4, '-1-29-30-', 0, 2, '编辑', 'PermissionList::edit', NULL, 1, 1, 0, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, '2021-01-17 14:03:19', '2021-01-17 14:03:19', NULL);
INSERT INTO `menus` VALUES (33, 30, 4, '-1-29-30-', 0, 2, '删除', 'PermissionList::delete', NULL, 1, 1, 0, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, '2021-01-17 14:03:34', '2021-01-17 14:03:34', NULL);

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (1, '2019_08_19_000000_create_failed_jobs_table', 1);
INSERT INTO `migrations` VALUES (2, '2021_01_16_061627_create_users_table', 1);
INSERT INTO `migrations` VALUES (3, '2021_01_17_052936_create_roles_table', 2);
INSERT INTO `migrations` VALUES (4, '2021_01_17_053127_create_role_user_table', 2);
INSERT INTO `migrations` VALUES (5, '2021_01_17_055751_create_menus_table', 3);
INSERT INTO `migrations` VALUES (6, '2021_01_17_060928_create_menu_role_table', 3);

-- ----------------------------
-- Table structure for role_user
-- ----------------------------
DROP TABLE IF EXISTS `role_user`;
CREATE TABLE `role_user`  (
  `role_id` bigint(20) UNSIGNED NOT NULL COMMENT '角色id',
  `user_id` bigint(20) UNSIGNED NOT NULL COMMENT '用户id'
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of role_user
-- ----------------------------
INSERT INTO `role_user` VALUES (1, 1);
INSERT INTO `role_user` VALUES (2, 1);

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '角色名称',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '备注',
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '1正常、0禁用',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES (1, '超级管理员', '拥有本系统的所有权限', 1, '2021-01-17 10:50:24', '2021-01-28 06:13:58', NULL);
INSERT INTO `roles` VALUES (2, '普通管理员', '普通的', 1, '2021-01-17 13:40:38', '2021-01-18 00:04:28', NULL);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '用户名/用户名',
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '密码',
  `name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT '姓名',
  `gender` tinyint(4) NOT NULL DEFAULT 1 COMMENT '1男2女',
  `mobile` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT '联系电话',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT '备注',
  `login_count` bigint(20) NOT NULL DEFAULT 0 COMMENT '登录次数',
  `last_login_time` timestamp(0) NULL DEFAULT NULL COMMENT '最后一次登录时间',
  `last_login_ip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '最后一次登录的ip地址',
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `users_username_unique`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'admin', '$2y$10$zC4Z4VThedTk3RlMpuUIH.qIYiurj5nPj5XexIdWz2pqX.XyocVFK', '刘义平', 1, '13417196510', '超级管理员', 73, '2021-01-28 08:10:22', '127.0.0.1', 1, '2021-01-16 14:28:35', '2021-01-28 08:10:22', NULL);

SET FOREIGN_KEY_CHECKS = 1;
